﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using DirectShowLib;

namespace BWWebCamRes
{
    class Program
    {

        // from http://stackoverflow.com/a/20418718/136771
        static private List<string> GetAllAvailableResolution(DsDevice vidDev)
        {
                int hr, bitCount = 0;
                IBaseFilter sourceFilter = null;
                var m_FilterGraph2 = new FilterGraph() as IFilterGraph2;
                hr = m_FilterGraph2.AddSourceFilterForMoniker(vidDev.Mon, null, vidDev.Name, out sourceFilter);
                var pRaw2 = DsFindPin.ByCategory(sourceFilter, PinCategory.Capture, 0);
                var AvailableResolutions = new List<string>();
                VideoInfoHeader v = new VideoInfoHeader();
                IEnumMediaTypes mediaTypeEnum;
                hr = pRaw2.EnumMediaTypes(out mediaTypeEnum);
                AMMediaType[] mediaTypes = new AMMediaType[1];
                IntPtr fetched = IntPtr.Zero;
                hr = mediaTypeEnum.Next(1, mediaTypes, fetched);
                while (fetched != null && mediaTypes[0] != null)
                {
                    Marshal.PtrToStructure(mediaTypes[0].formatPtr, v);
                    if (v.BmiHeader.Size != 0 && v.BmiHeader.BitCount != 0)
                    {
                        if (v.BmiHeader.BitCount > bitCount)
                        {
                            AvailableResolutions.Clear();
                            bitCount = v.BmiHeader.BitCount;
                        }
                        AvailableResolutions.Add(v.BmiHeader.Width + "x" + v.BmiHeader.Height+" Color depth: "+v.BmiHeader.BitCount);
                    }
                    hr = mediaTypeEnum.Next(1, mediaTypes, fetched);
                }
                return AvailableResolutions;
        }

        static void Main(string[] args)
        {
            DsDevice[] capDevices;
            capDevices = DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice);
            if (capDevices.Length == 0)
            {
                Console.WriteLine("No cameras detected");
                //Could not found camera
            }
            else
            {
                for (var i = 0; i < capDevices.Length; i++)
                {
                    string myCameraName = capDevices[i].Name.ToString();
                    Console.WriteLine("Camera \"{0}\" available resolutions: ", myCameraName);
                    try
                    {
                        List<string> resolutions = GetAllAvailableResolution(capDevices[i]);
                        foreach (string resolution in resolutions)
                        {
                            Console.WriteLine(" |- {0}", resolution);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(" |- Unable to read resolutions: {0}", ex.Message);
                    }
                }
                Console.WriteLine("End");
            } 
        }


    }
}